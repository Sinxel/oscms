<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>CMS</title>
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="//cdn.materialdesignicons.com/3.7.95/css/materialdesignicons.min.css">
        <link href="{{ asset('css/app.css') }}" type="text/css" rel="stylesheet" />
        <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
        <meta name="csrf-token" value="{{ csrf_token() }}" />
    </head>
    
    <body>
    @if($message!='')
    <script>
        alert(' {{ $message }}')
    </script>
    @endif
        <div id="app">

            <header-menu loggedin="{{ Auth::check() ? true : false }}"></header-menu>
           
            <div class="separator-100"></div>
             <header-section></header-section>
        <div class="separator-30"></div>
            @foreach($pages as $k=>$page)
                @if($k%2==0)
                    <left-preview title="{{ $page->title }}" description="{{ $page->description }}" image="{{ $page->image }}"></left-preview>
                    <div class="separator-30"></div>
                @else
                    <right-preview title="{{ $page->title }}" description="{{ $page->description }}" image="{{ $page->image }}"></right-preview>
                    <div class="separator-30"></div>
                @endif
            @endforeach

        




            <div class="separator-100">

            </div>
            <div class="container">
                <center>
                    <h6>Our Blog</h6>
                    <h4>Latest News</h4>
                </center>
            </div>
            <div class="container ">
                <div class="row">
                    
                    @foreach($news as $nhl)
                        
                        <news title="{{ $nhl->title }}" image="{{ $nhl->image }}" description="{{ $nhl->description }}"></news>
                    @endforeach
                    
                </div>
            </div>
            <div class="separator-100"></div>
            <div class="contact-container">
                <center><h4>Contact Us</h4></center>
                <div class="container">
                    <div class="row">
                        <contact icon="phone" title="Phone :" content="{{ $contact->phone }}"></contact>
                        <contact icon="email" title="Email :" content="{{ $contact->email }}"></contact>
                        <contact icon="map-marker" title="Address :" content="{{ $contact->address }}"></contact>
                    </div>
                </div>
            </div>
        </div><!-- APP -->

        
        <script src="{{ mix('js/app.js') }}" type="text/javascript"></script>
    </body>
</html>
